import json
from yaml import safe_load, YAMLError
from faker import Faker

faker = Faker()


def load_config(config_file):
    with open(config_file, 'r') as stream:
        try:
            return safe_load(stream)
        except YAMLError as exc:
            print(exc)


def json_generator():
    return json.dumps({'name': faker.name()})
