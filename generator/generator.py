from time import sleep
from helpers import load_config, json_generator
from RBMQhendler import RabbitHandler

if __name__ == '__main__':
    config = load_config('config.yml')

    rabbitmq_host = config['rabbitmq_host']
    rabbitmq_username = config['rabbitmq_username']
    rabbitmq_password = config['rabbitmq_password']
    rabbitmq_exchange = config['rabbitmq_exchange']
    rabbitmq_routing_key = config['rabbitmq_routing_key']
    sleep_time = 1 / config['count_per_second']

    rbmq = RabbitHandler(rabbitmq_host, exchange=rabbitmq_exchange, routing_key=rabbitmq_routing_key,
                         username=rabbitmq_username, password=rabbitmq_password)

    while 1:
        json = json_generator()
       # print(json)
        rbmq.emit(json)
        sleep(sleep_time)
