import threading
import pika
from datetime import datetime


class RabbitHandler:

    def __init__(self, host, port=None, virtual_host=None, username=None, password=None, exchange='', routing_key=''):

        self.connection_para = dict(host=host)
        if port:
            self.connection_para['port'] = port
        if virtual_host:
            self.connection_para['virtual_host'] = virtual_host
        if username and password:
            self.connection_para['credentials'] = pika.credentials.PlainCredentials(username, password)
        self.exchange = exchange
        self.routing_key = routing_key
        self.connection, self.channel = None, None

        self.is_exchange_declared = False
        self.emit_lock = threading.Lock()
        self.connect()

    def connect(self):

        self.connection_para['heartbeat_interval'] = 0
        self.connection = pika.BlockingConnection(pika.ConnectionParameters(**self.connection_para))
        self.channel = self.connection.channel()
        print('%s - stdout - [mq] Connect success.' % datetime.now().strftime("%Y-%m-%d %H:%M:%S"))
        if not self.is_exchange_declared:
            self.channel.exchange_declare(exchange=self.exchange, durable=True, auto_delete=False)
            self.channel.queue_declare(queue=self.exchange, durable=True)
            self.is_exchange_declared = True

    def emit(self, record):
        self.emit_lock.acquire()
        try:
            if not self.connection or not self.channel:
                self.connect()
            self.channel.basic_publish(exchange=self.exchange,
                                       routing_key=self.routing_key,
                                       body=record,
                                       properties=pika.BasicProperties(
                                           delivery_mode=2,
                                       ))
        except Exception:
            self.channel, self.connection = None, None
        finally:
            self.emit_lock.release()

    def close(self):
        """
        clear when closing
        """
        try:
            if self.channel:
                self.channel.close()
            if self.connection:
                self.connection.close()
                print('%s - stdout - [mq] Clean up success.' % datetime.now().strftime("%Y-%m-%d %H:%M:%S"))
        finally:
            print('Close')
