import pika
import xmlrpc.client
import json

from helpers import load_config


if __name__ == '__main__':
    config = load_config('config.yml')

    url = config['xmlrpc_url']
    odoo_db = config['odoo_db']
    odoo_user = config['odoo_user']
    odoo_password = config['odoo_password']

    rabbitmq_host = config['rabbitmq_host']
    rabbitmq_username = config['rabbitmq_username']
    c = config['rabbitmq_password']
    rabbitmq_exchange = config['rabbitmq_exchange']
    rabbitmq_routing_key = config['rabbitmq_routing_key']

    info = xmlrpc.client.ServerProxy('{}/xmlrpc/2/common'.format(url))
    uid = info.authenticate(odoo_db, odoo_user, odoo_password, {})

    connection_para = dict(host=rabbitmq_host,
                           credentials=pika.credentials.PlainCredentials(rabbitmq_username, rabbitmq_username))
    connection = pika.BlockingConnection(pika.ConnectionParameters(**connection_para))
    channel = connection.channel()
    channel.exchange_declare(exchange=rabbitmq_exchange, exchange_type='direct', durable=True)
    channel.queue_declare(exclusive=True, durable=True)
    channel.queue_bind(exchange=rabbitmq_exchange, queue=rabbitmq_routing_key)

    def callback(ch, method, properties, body):
        msg = json.loads(body.decode("utf-8"))

        models = xmlrpc.client.ServerProxy('{}/xmlrpc/2/object'.format(url))
        models.execute_kw(odoo_db, uid, odoo_password, 'res.partner', 'create', [{
            'name': msg['name'],
        }])
        count_users = models.execute_kw(odoo_db, uid, odoo_password, 'res.partner', 'search_count', [[]])

        print("Create user with name {}".format(msg['name']))
        print("Count users {}".format(count_users))

        ch.basic_ack(delivery_tag=method.delivery_tag)


    channel.basic_qos(prefetch_count=1)
    channel.basic_consume(callback, queue=rabbitmq_routing_key, no_ack=False)

    channel.start_consuming()
