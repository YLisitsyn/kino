from yaml import safe_load, YAMLError


def load_config(config_file):
    with open(config_file, 'r') as stream:
        try:
            return safe_load(stream)
        except YAMLError as exc:
            print(exc)
